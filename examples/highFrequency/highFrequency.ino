/*
 * This example sets the pins 9 and 10 frequency to 32kHz and pins 3 and 11 frequency to 4kHz
 * and outputs a PWM with a 50% duty cycle.
 */
#include <PWMFreq.h>

void setup() {
  setPWMFrequency(PINS_9_10, PWM_32k);
  setPWMFrequency(PINS_3_11, PWM_4k);
  analogWrite(9,128);
  analogWrite(3,128);
}

void loop() {
}
